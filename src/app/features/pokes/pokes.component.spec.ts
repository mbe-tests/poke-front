import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { Pokemon } from './pokes';

import { PokesComponent } from './pokes.component';
import { PokeService } from './pokes.service';

describe('PokesComponent', () => {
  let component: PokesComponent;
  let fixture: ComponentFixture<PokesComponent>;
  let pokeServiceSpy: jasmine.SpyObj<PokeService>;

  const spy = jasmine.createSpyObj('PokeService', ['getPokes']);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        { provide: PokeService, useValue: spy }
      ],
      declarations: [PokesComponent]
    })
      .compileComponents();
    pokeServiceSpy = TestBed.inject(PokeService) as jasmine.SpyObj<PokeService>;
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PokesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit', () => {
    pokeServiceSpy.getPokes.and.returnValue(of({} as Pokemon))

    component.ngOnInit();

    expect(pokeServiceSpy.getPokes).toHaveBeenCalledWith(0);
  });
  
  it('next', () => {
    pokeServiceSpy.getPokes.and.returnValue(of({} as Pokemon))

    component.next();

    expect(component.index).toBe(1);
    expect(pokeServiceSpy.getPokes).toHaveBeenCalledWith(1);
  });

  it('previous', () => {
    component.index = 4;
    pokeServiceSpy.getPokes.and.returnValue(of({} as Pokemon))

    component.previous();

    expect(component.index).toBe(3);
    expect(pokeServiceSpy.getPokes).toHaveBeenCalledWith(0);
  });
});
