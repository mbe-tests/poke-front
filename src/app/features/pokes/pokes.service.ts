import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Poke, PokeEvolution, Pokemon, PokeSpecies } from './pokes';
import { Observable, switchMap } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PokeService {
  pokeUrl = 'https://pokeapi.co/api/v2/';

  constructor(private http: HttpClient) { }

  getPokes(index: number): Observable<Pokemon> {
    const url = `${this.pokeUrl}pokemon?offset=${index * 20}&limit=20`; 
    return this.http.get<Pokemon>(url);
  }

  getPoke(id: string): Observable<Poke>  {
    const url = `${this.pokeUrl}pokemon/${id}`; 
    return this.http.get<Poke>(url);
  }

  getEvo(url: string): Observable<PokeEvolution> {
    return this.http.get<PokeSpecies>(url).pipe(
      switchMap(data => this.http.get<PokeEvolution>(data.evolution_chain.url))
    )
  }
}
