import { Component, Input, OnInit } from '@angular/core';
import { of } from 'rxjs';
import { Poke } from '../../pokes';
import { PokeService } from '../../pokes.service';

@Component({
  selector: 'app-poke-item',
  templateUrl: './poke-item.component.html',
  styleUrls: ['./poke-item.component.scss']
})
export class PokeItemComponent{
  @Input()
  poke!: Poke;
}
