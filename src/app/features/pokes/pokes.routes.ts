import { Routes } from '@angular/router';

import { PokeComponent } from './poke/poke.component';
import { PokesComponent } from './pokes.component';

export const PokeRoutes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'poke'
  },
  {
    path: 'poke',
    component: PokesComponent
  },
  {
    path: 'poke/:id',
    component: PokeComponent
  }
];

  