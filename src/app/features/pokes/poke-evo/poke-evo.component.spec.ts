import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';

import { PokeEvolution } from '../pokes';
import { PokeService } from '../pokes.service';
import { PokeEvoComponent } from './poke-evo.component';

describe('PokeEvoComponent', () => {
  let component: PokeEvoComponent;
  let fixture: ComponentFixture<PokeEvoComponent>;
  let pokeServiceSpy: jasmine.SpyObj<PokeService>;

  const spy = jasmine.createSpyObj('PokeService', ['getEvo']);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        { provide: PokeService, useValue: spy }
      ],
      declarations: [ PokeEvoComponent ]
    })
    .compileComponents();   
    pokeServiceSpy = TestBed.inject(PokeService) as jasmine.SpyObj<PokeService>;

  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PokeEvoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  
  it('ngOnInit', () => {
    const fakeUrl = "FAKE_URL";
    component.url = fakeUrl;
    pokeServiceSpy.getEvo.and.returnValue(of({ chain: { species: {}}} as PokeEvolution))

    component.ngOnInit();

    expect(pokeServiceSpy.getEvo).toHaveBeenCalledWith(fakeUrl);
  });
});
