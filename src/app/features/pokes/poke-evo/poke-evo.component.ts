import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { PokeEvolution } from '../pokes';
import { PokeService } from '../pokes.service';

@Component({
  selector: 'app-poke-evo',
  templateUrl: './poke-evo.component.html',
  styleUrls: ['./poke-evo.component.scss']
})
export class PokeEvoComponent implements OnInit {

  @Input()
  url!: string;
  $evos!: Observable<PokeEvolution>;

  constructor(public pokeService: PokeService) { }

  ngOnInit(): void {
    this.$evos = this.pokeService.getEvo(this.url);
  }

}
