import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { PokeService } from './pokes.service';
import { Poke, PokeEvolution, Pokemon, PokeSpecies } from './pokes';

describe('PokeService', () => {
  let service: PokeService;
  let httpMock: HttpTestingController;
  const pokeUrl = 'https://pokeapi.co/api/v2/';

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [PokeService]
    });
    service = TestBed.inject(PokeService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('getPokes', () => {
    const pokemon = {} as Pokemon;
    service.getPokes(0).subscribe(data => {
      expect(data).toEqual(pokemon);
    });
    const request = httpMock.expectOne(`${pokeUrl}pokemon?offset=0&limit=20`);
    expect(request.request.method).toBe('GET');
    request.flush(pokemon);
  });

  it('getPoke', () => {
    const poke = {} as Poke;
    service.getPoke('FAKE').subscribe(data => {
      expect(data).toEqual(poke);
    });
    const request = httpMock.expectOne(`${pokeUrl}pokemon/FAKE`);
    expect(request.request.method).toBe('GET');
    request.flush(poke);
  });

  it('getEvo', () => {
    const pokeSpecies = { evolution_chain: { url: 'FAKE_URL_2' } } as PokeSpecies;
    const pokeEvo = {} as PokeEvolution;
    service.getEvo('FAKE_URL').subscribe(data => {
      expect(data).toEqual(pokeEvo);
    });

    const request = httpMock.expectOne('FAKE_URL');
    expect(request.request.method).toBe('GET');
    request.flush(pokeSpecies);

    const request2 = httpMock.expectOne('FAKE_URL_2');
    expect(request2.request.method).toBe('GET');
    request2.flush(pokeEvo);
  });
});
