import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Pokemon } from './pokes';
import { PokeService } from './pokes.service';

@Component({
  selector: 'app-pokes',
  templateUrl: './pokes.component.html',
  styleUrls: ['./pokes.component.scss']
})
export class PokesComponent implements OnInit {

  $pokes!: Observable<Pokemon>;
  index= 0;

  constructor(public pokeService: PokeService) {   }

  ngOnInit(): void {
    this.$pokes = this.pokeService.getPokes(this.index)
  }

  next(): void {
    this.index = this.index + 1;
    this.$pokes = this.pokeService.getPokes(this.index);
  }

  previous(): void {
    this.index = this.index - 1;
    this.$pokes = this.pokeService.getPokes(this.index);
  }

}
