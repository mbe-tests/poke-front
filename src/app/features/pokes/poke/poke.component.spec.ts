import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { Poke } from '../pokes';
import { PokeService } from '../pokes.service';
import { PokeComponent } from './poke.component';

describe('PokeComponent', () => {
  let component: PokeComponent;
  let fixture: ComponentFixture<PokeComponent>;
  let pokeServiceSpy: jasmine.SpyObj<PokeService>;
  let activatedRouteSpy: jasmine.SpyObj<ActivatedRoute>;

  const spy = jasmine.createSpyObj('PokeService', ['getPoke']);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        {
          provide: ActivatedRoute, useValue: {
            params: of({ id: "2" }),
          }
        },
        { provide: PokeService, useValue: spy }
      ],
      declarations: [PokeComponent]
    })
      .compileComponents();
    activatedRouteSpy = TestBed.inject(ActivatedRoute) as jasmine.SpyObj<ActivatedRoute>;
    pokeServiceSpy = TestBed.inject(PokeService) as jasmine.SpyObj<PokeService>;
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PokeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit', () => {
    pokeServiceSpy.getPoke.and.returnValue(of({ sprites: {}, species: {}} as Poke))

    component.ngOnInit();

    expect(component.id).toBe("2");
    expect(pokeServiceSpy.getPoke).toHaveBeenCalledWith("2");
  });
  
});
