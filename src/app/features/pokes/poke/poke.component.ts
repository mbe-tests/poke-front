import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Poke } from '../pokes';
import { PokeService } from '../pokes.service';

@Component({
  selector: 'app-poke',
  templateUrl: './poke.component.html',
  styleUrls: ['./poke.component.scss']
})
export class PokeComponent implements OnInit {

  $poke!: Observable<Poke>;
  id!: string;

  constructor(private route: ActivatedRoute, private pokeService: PokeService) {   }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.id = params['id'];
      this.$poke = this.pokeService.getPoke(this.id);
    });
  }

}
