interface PokeTypes {
    slot: number,
    type: PokeType 
}

interface PokeType {
    name: string;
    url: string;
}

interface PokeSprites {
    front_default: string;
}

interface PokeEvolutionChain {
    url: string;
    chain: PokeEvolutionChain;
}

interface PokeEvolutionChain {
    evolves_to: Array<PokeEvolutionChain>;
    species: PokeSpecies;
}

export interface PokeEvolution {
    baby_trigger_item: string;
    chain: PokeEvolutionChain;
    id: number;
}

export interface PokeSpecies {
    name: string;
    evolution_chain: PokeEvolutionChain;
    url: string;
}

export interface Poke {
    name: string;
    url: string;
    height: string;
    weight: string;
    types: Array<PokeTypes>;
    sprites: PokeSprites;
    species: PokeSpecies;
}

export interface Pokemon {
    count: number;
    next: string;
    previous: string;
    results: Array<Poke>;
}