import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';

import { PokesComponent } from './pokes.component';
import { PokeRoutes } from './pokes.routes';
import { PokeItemComponent } from './poke-item/poke-item/poke-item.component';
import { PokeComponent } from './poke/poke.component';
import { PokeEvoComponent } from './poke-evo/poke-evo.component';

@NgModule({
  declarations: [PokesComponent, PokeItemComponent, PokeComponent, PokeEvoComponent],
  imports: [CommonModule, MatCardModule, MatButtonModule, MatIconModule, RouterModule.forChild(PokeRoutes)]

})
export class PokeModule { }
